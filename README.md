# Telegram bot for german news

This is small project, that parses some RSS Feeds of German news sources and retransmits to Telegram Channel.

To start app
```
python3 main.py
```

Setup Virtual Environment:
```
python3 -m venv /path/to/new/virtual/environment
```

Activate Virtual environment:
```
/path/to/new/virtual/environment/Scripts/activate
```

Install all dependecies:
```
pip3 install -r requirements.txt
```