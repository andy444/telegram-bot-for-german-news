from config import environments
import telegram

bot = telegram.Bot(token=environments.get('TELEGRAM_KEY'))
chat_id = environments.get('CHANNEL_LINK')

def format_text(entry):
  source = entry.get('source')
  title = entry.get('title')
  summary = entry.get('summary')
  link = entry.get('link')
  image = entry.get('image')
  formated_text = f'_{source}_\n*{title}*\n\n{summary if summary is not None else ""}\n\n{link}'
  return formated_text

def send_to_channel(entries):
  for entry in entries:
    formated_text = format_text(entry)
    bot.send_message(chat_id=chat_id, text=formated_text, parse_mode= 'Markdown')
