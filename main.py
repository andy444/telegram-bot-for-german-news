import feed_parser
from bot_handler import send_to_channel
from time import sleep

def update_routine():
  print('Program is started, waiting for news...')
  while True:
    fresh_news = feed_parser.get_fresh_news()
    send_to_channel(fresh_news)
    print(fresh_news)
    sleep(60)

if __name__ == '__main__':
  update_routine()