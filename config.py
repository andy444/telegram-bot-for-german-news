from dotenv import load_dotenv
import os
from dateutil.parser import parse
load_dotenv()

class FeedSource():
  def __init__(self, publisher_name, link):
    self.publisher_name = publisher_name
    self.link = link
    self.last_news_time = None

  def set_last_news_time(self, last_news_time):
    if isinstance(last_news_time, str):
      last_news_time = parse(last_news_time)
    if self.last_news_time is None or self.last_news_time < last_news_time:
      self.last_news_time = last_news_time

sources = [
  FeedSource('Tagesspigel', 'https://www.tagesspiegel.de/contentexport/feed/home'),
  FeedSource('Berliner Zeitung', 'https://www.berliner-zeitung.de/feed.xml'),
  FeedSource('Neues Deutschland', 'https://www.neues-deutschland.de/rss/neues-deutschland.xml'),
  FeedSource('Der Freitag', 'https://www.freitag.de/@@RSS')
]

environments = {
  'TELEGRAM_KEY': os.getenv('TELEGRAM_KEY'),
  'CHANNEL_LINK': os.getenv('CHANNEL_LINK')
}