from datetime import datetime
import unittest
import feed_parser
import pytz
from datetime import datetime

class TestFeedParser(unittest.TestCase):
  tz = pytz.timezone('CET')
  test_date = datetime(2021, 3, 13, 14, 0, 0, 0, tz)
  
  def test_new_entry_exists(self):
    result = feed_parser.compare_time(self.test_date, 'Sat, 13 Mar 2021 13:15:00 GMT')
    self.assertEqual(result, True)

  def test_new_entry_doesnt_exists(self):
    result = feed_parser.compare_time(self.test_date, 'Sat, 13 Mar 2021 12:45:00 GMT')
    self.assertEqual(result, False)

  def test_strip_html(self):
    html_text = '<html><head><title>Test &amp;</title></head>'
    stripped_text = 'Test &'
    result = feed_parser.strip_html_text(html_text)
    self.assertEqual(result, stripped_text)

  def test_links_for_jpg_if_exists(self):
    links = [{'src': 'www.test.com/test.html', 'src': 'https://www.test.com/test.jpg'}]
    result = feed_parser.check_links_for_images(links)
    self.assertEqual(result, 'https://www.test.com/test.jpg')

  def test_links_for_png_if_exists(self):
    links = [{'src': 'www.test.com/test.html', 'src': 'http://www.test.com/test.png'}]
    result = feed_parser.check_links_for_images(links)
    self.assertEqual(result, 'http://www.test.com/test.png')

  def test_links_for_image_if_not_exists(self):
    links = [{'src': 'www.test.com/test.html'}]
    result = feed_parser.check_links_for_images(links)
    self.assertEqual(result, None)

if __name__ == '__main__':
    unittest.main()