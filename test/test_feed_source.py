import unittest
from config import FeedSource
import datetime
from dateutil.tz import tzutc

class TestBotHandler(unittest.TestCase):
  def test_set_date(self):
    fs = FeedSource('Brand News', 'https://www.brandnews.com/rss')
    fs.set_last_news_time('Sat, 13 Mar 2021 13:15:00 GMT')
    self.assertEqual(fs.last_news_time, datetime.datetime(2021, 3, 13, 13, 15, tzinfo=tzutc()))

  def test_set_date_fresher(self):
    fs = FeedSource('Brand News', 'https://www.brandnews.com/rss')
    fs.set_last_news_time('Sat, 13 Mar 2021 13:15:00 GMT')
    fs.set_last_news_time('Sat, 13 Mar 2021 13:20:00 GMT')
    self.assertEqual(fs.last_news_time, datetime.datetime(2021, 3, 13, 13, 20, tzinfo=tzutc()))

  def test_set_date_older(self):
    fs = FeedSource('Brand News', 'https://www.brandnews.com/rss')
    fs.set_last_news_time('Sat, 13 Mar 2021 13:15:00 GMT')
    fs.set_last_news_time('Sat, 13 Mar 2021 13:10:00 GMT')
    self.assertEqual(fs.last_news_time, datetime.datetime(2021, 3, 13, 13, 15, tzinfo=tzutc()))