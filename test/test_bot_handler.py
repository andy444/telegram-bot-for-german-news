import unittest
import bot_handler

class TestBotHandler(unittest.TestCase):
  def test_format_text(self):
    mock_entry = {
      'source': 'Source',
      'title': 'Title',
      'link': 'https://www.test.com/news/1234',
      'published': 'Sat, 13 Mar 2021 13:15:00 GMT',
      'summary': 'Short description',
      'image': 'https//www.test.com/news/1234/image.jpg'
    }
    expected_result = f'_Source_\n*Title*\n\nShort description\n\nhttps://www.test.com/news/1234'
    result = bot_handler.format_text(mock_entry)
    self.assertEqual(result, expected_result)

  def test_format_text_without_summary(self):
    mock_entry = {
      'source': 'Source',
      'title': 'Title',
      'link': 'https://www.test.com/news/1234',
      'published': 'Sat, 13 Mar 2021 13:15:00 GMT',
      'summary': None,
      'image': 'https//www.test.com/news/1234/image.jpg'
    }
    expected_result = f'_Source_\n*Title*\n\n\n\nhttps://www.test.com/news/1234'
    result = bot_handler.format_text(mock_entry)
    self.assertEqual(result, expected_result)

  def test_format_text_without_summary_and_image(self):
    mock_entry = {
      'source': 'Source',
      'title': 'Title',
      'link': 'https://www.test.com/news/1234',
      'published': 'Sat, 13 Mar 2021 13:15:00 GMT',
      'summary': None,
      'image': None
    }
    expected_result = f'_Source_\n*Title*\n\n\n\nhttps://www.test.com/news/1234'
    result = bot_handler.format_text(mock_entry)
    self.assertEqual(result, expected_result)

if __name__ == '__main__':
    unittest.main()