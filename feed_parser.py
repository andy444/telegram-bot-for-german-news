import feedparser
import html
import re
from config import sources
from dateutil.parser import parse

def strip_html_text(text):
  text_without_html = re.sub('<[^<]+?>', '', text)
  return html.unescape(text_without_html)

def get_last_time_published_news(source):
  last_published_news_time = None
  for entry in source:
    entry_published = parse(entry.published)
    if last_published_news_time is None or last_published_news_time < entry_published:
      last_published_news_time = entry_published
  return last_published_news_time

def compare_time(last_updated, published_time):
  if parse(published_time) > last_updated:
    return True
  else:
    return False

def check_links_for_images(links):
  regex_pattern = r'(?:http\:|https\:)?\/\/.*\.(?:png|jpg|jpeg)'
  if len(links) < 1:
    return None
  for link in links:
    for key, value in link.items():
      match = re.findall(regex_pattern, value)
      if match:
        return match[0]

def create_news_dict(source, entry):
  return {
    'source': source,
    'title': entry.title,
    'link': entry.link,
    'published': entry.published,
    'summary': strip_html_text(entry.summary) if 'summary' in entry else None,
    'image': check_links_for_images(entry.links)
  }

def get_fresh_news():
  fresh_news = []
  for source in sources:
    d = feedparser.parse(source.link)
    feed_title = d.feed.title
    last_time_published_news = get_last_time_published_news(d.entries)
    if source.last_news_time is None: 
      source.last_news_time = last_time_published_news
    if source.last_news_time < last_time_published_news:
      for entry in d.entries:
        is_fresh = compare_time(source.last_news_time, entry.published)
        if is_fresh:
          news = create_news_dict(feed_title, entry)
          fresh_news.append(news)
    source.set_last_news_time(last_time_published_news)
  return fresh_news


